# SMS Charging Module
Basic destination-address filtering. If the destination address is allowed, then the message is submitted for delivery to the correct destination; otherwise, the message is rejected.

## Design
- pub-sub design using using Kafka
- Improve performance using Async Nonblocking logging with Kafka, Which can feed the log streams to elasticsearch and then to kibana. Somehow this configuration is not working. Need more time to fix this. 
- Due to time limit I did not focused on testing part (Test is must for any application)
- Load test result for 10000 SMS
Start Time 2021-11-11 22:49:36.066
End Time 2021-11-11 22:49:44.144
Total Time 8.08 Seconds




## Features

- Micro-services architecture: including a pub-sub design
- Able to filter 1000 messages per second
- Basic destination-address filtering. If the destination address is allowed, then the message is submitted for delivery to the correct destination; otherwise, the message is rejected.
- The SMS Gateway should still submit messages (fire-and-forget) to the charging module irrespective of the spam filter`s outcome.



## Tech
Used open source code:

- [Java 11] - Backend Development
- [Spring Boot] - Microservices
- [Kafka] - As Pub Sub archetecture.
- [Spring Cloud Stream] - Microservices
- [Kafka Stream] - Microservices
- [Jmeter] - Load Testing
- [log4j] - Async Nonblocking logging with Kafka(Can feed stream to elasticsearch)

## Installation

requires Java 11, Maven and Kafka server to run the application.

Install the maven dependencies to start the application.

```sh
Setup Kafka follow steps provided by [confluent](https://www.confluent.io/) Or [Apache Kafka](https://kafka.apache.org/)
Go to project directory and execute below steps
mvn clean install
java -jar sms-gateway.jar
```

