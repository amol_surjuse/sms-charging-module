package com.haud.sms.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message implements Serializable {
	private static final long serialVersionUID = 4883606065564314902L;
	private String body;
	private String to;
	private String from;
}
