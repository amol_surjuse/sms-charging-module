package com.haud.charging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class SmsChargingModuleApplication {
	public static void main(String[] args) {
		SpringApplication.run(SmsChargingModuleApplication.class, args);
	}
}
