package com.haud.charging.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.haud.sms.model.Message;

@Service
public class MessageService {
	private static final Logger LOGGER = LogManager.getLogger(MessageService.class);
	
	@Async
	public void processSms(Message message) {
		LOGGER.info("Started processing message {}", message);
	}

}
