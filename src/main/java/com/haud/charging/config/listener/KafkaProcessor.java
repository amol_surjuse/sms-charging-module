package com.haud.charging.config.listener;

import java.util.function.Consumer;

import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.haud.charging.service.MessageService;
import com.haud.sms.model.Message;

@Configuration
public class KafkaProcessor {
	@Autowired
	private MessageService messageService;

	@Bean
	public Consumer<KStream<String, Message>> smsCharging() {
		return kStream -> kStream.foreach((key, message) -> {
			messageService.processSms(message);
		});
	}
}
